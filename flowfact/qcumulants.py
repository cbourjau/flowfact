import numpy as np


class Qcumulants():
    def __init__(self, max_n):
        self.n = np.arange(max_n)
        self.nominator = np.zeros(shape=max_n)
        self.sum_of_weights = np.zeros(shape=max_n)

    def _calc_Qn_square(self, phis):
        """
        Given all the phi values of an event, calculate all |Q_n^2| up to
        `self.max_n` and store it internally
        """
        return (phis.size**2 *
                (np.mean(np.cos(self.n[None, :] * phis[:, None]), axis=0)**2 +
                 np.mean(np.sin(self.n[None, :] * phis[:, None]), axis=0)**2))

    def _calc_weights(self, nparts):
        """
        Calculate the two-particle weight of an event with the given
        number of particles
        """
        return nparts * (nparts - 1)

    def process_event(self, phis):
        """
        Calculate the nominator and denominator sums of <<2>>
        """
        self.nominator += self._calc_Qn_square(phis) - phis.size
        self.sum_of_weights += phis.size * (phis.size - 1)
