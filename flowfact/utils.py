import numpy as np


def wrap_2pi(a):
    return (a) % (2 * np.pi)


def normalize_multiplet(multiplet_pdf):
    """
    Normalize the given multiplet PDF such that its integral (not sum of bins!) is 1
    """
    nbins = multiplet_pdf.shape[0]
    ndim = multiplet_pdf.ndim
    h = np.copy(multiplet_pdf)
    h /= np.sum(multiplet_pdf)
    h /= (2 * np.pi / nbins)**ndim
    return h


def correct_segmentation(nbins):
    """
    Correction factors to v_n based on the number of bins in covering the [0, 2pi) interval

    Parameters
    ----------
    nbins : int
        Number of bins in azimuthal dimension

    Returns
    -------
    np.array : Correction factors for the maximal number of v_ns starting at v_0
    """
    ret = np.ones(nbins)
    n = np.arange(1, nbins)
    ret[1:] = np.sin(np.pi * n / nbins) / (np.pi * n / nbins)
    return ret
