import numpy as np


def fourier_decompose(single_pdf):
    """
    Extract the amplitudes and phases of all possible Fourier componets

    Parameters
    ----------
    single_pdf : np.array
        A single particle distribution

    Returns
    -------
    (np.array, np.array) :
        Amplitudes and phases for each mode
    """
    # Yields imaginary frequencies!
    # rfft expects the sum of the array to be 1, not the integral
    # (since it does not know about the bin width!
    freqs = np.fft.rfft(single_pdf / np.sum(single_pdf))
    return (np.abs(freqs), np.arctan2(np.real(freqs), np.imag(freqs)))


# Old method:
# def fit_pdf(paras):
#     #v = np.array([0.5, 0.0, 0.4])
#     #psi = np.array([0, 0, 0])
#     x = np.linspace(0, 2* np.pi, 20)
#     psi = paras[-1]
#     v = paras[:-1]
#     #print [2 * v[n] * np.cos(n * (x - psi)) for n in range(len(v))]
#     bin_width = 1 # 20 / 2 / np.pi
#     #ret = 1.0 / 2 / np.pi * (v[0] + np.sum([2 * v[n] * np.cos(n * (x - psi))
#     #                                     for n in range(1, len(v))], axis=0)) / bin_width
#     ret = 1.0 / 2 / np.pi * (1 + np.sum([2 * v[n] * np.cos(n * (x - psi))
#                                          for n in range(1, len(v))], axis=0)) / bin_width
#     return ret

# def vn_fit(paras, measured):
#     return fit_pdf(paras) - measured
