"""
Functions creating multi-variate distributions and random variables
"""
from generators import Flow_gen
import numpy as np


_flow = flowfact.Flow_gen(v, psi, 0)
v_mod = lambda in_v: in_v  # in_v * np.random.uniform(0.5, 1.5, size=in_v.size)
psi_mod = lambda in_psi: in_psi
azi = lambda: np.pi

def mv_pdf(flow_gen, a, phi1, phi2):
    return (flow_gen.pdf_with_parameters(x=wrap_2pi(phi1 + a), v=v_mod, psi=psi_mod, azimuth=azi)
           * flow_gen.pdf_with_parameters(x=wrap_2pi(phi2 + a), v=v_mod, psi=psi_mod, azimuth=azi)
           * (1.0/2/np.pi))

def mv_non_flow_pdf(a, phi1, phi2):
    phi1 = np.array(phi1)
    phi2 = np.array(phi2)
    pos = np.array([wrap_2pi(phi1 + a), wrap_2pi(phi2 + a)])
    
    circular_grid = np.mgrid[-4:6:2, -4:6:2].T
    rv = multivariate_normal([0, 0], [[0.09, 0.06], [0.06, 0.09]])
    
    return np.sum(rv.pdf(pos + circular_grid * np.pi))

def mv_non_flow_rv(size):
    rv = multivariate_normal([0, 0], [[0.09, 0.06], [0.06, 0.09]])
    return wrap_2pi(rv.rvs(size=size))
