import numpy as np
from scipy.stats import rv_continuous

from utils import wrap_2pi


class Flow_gen(rv_continuous):
    """Flow like distribution of particles"""

    def __init__(self, v, psi, seed=None, **kwargs):
        self.v = np.copy(v).astype(np.float64)
        self.psi = np.copy(psi).astype(np.float64)
        self._ns = np.arange(1, len(v) + 1)
        if seed:
            np.random.seed(seed * 1000)
        super(Flow_gen, self).__init__(a=0, b=2 * np.pi, **kwargs)

    def _pdf(self, x):
        # Note: The pdf has to be normlizable, or everything goes bananas!
        sum_part = 2 * self.v[:, None] * np.cos(self._ns[:, None] * (x - self.psi[:, None]))
        return 1.0 / 2 / np.pi * (1 + np.sum(sum_part, axis=0))

    def _cdf(self, x):
        "cdf for 0 to x"
        sumpart = (2 * self.v[:, None] *
                   np.sin((x - self.psi[:, None]) * self._ns[:, None]) /
                   self._ns[:, None])
        return (1.0 / 2 / np.pi * (x + np.sum(sumpart, axis=0)))

    def rvs_with_parameters(self,
                            v=lambda mean_v: np.random.normal(mean_v, 0.5 * mean_v),
                            psi=lambda mean_psi: mean_psi + np.random.uniform(low=-np.pi, high=np.pi, size=mean_psi.size),
                            *args, **kwargs):
        """
        Calls rvs with extra parameters. This is needed since
        parameters must either be scalars or broadcasted; arrays
        cannot be used as parameters in rvs()

        Parameters
        ----------
        v : callable
            Takes the default (mean) `v`s and modifies them for this call
        psi : callable
            Takes the default (mean) `psi`s and modifies them for this call
        args, kwargs :
            Passed on directly to self.rvs()

        Returns
        -------
        np.array : Result from self.rvs
        """
        old_v, old_psi = self.v, self.psi
        self.v, self.psi = v(self.v), psi(self.psi)
        self._ns = np.arange(1, len(self.v) + 1)
        res = self.rvs(*args, **kwargs)
        self.v, self.psi = old_v, old_psi
        self._ns = np.arange(1, len(self.v) + 1)
        return wrap_2pi(res)

    def pdf_with_parameters(self,
                            v=lambda mean_v: np.random.normal(mean_v, 0.5 * mean_v),
                            psi=lambda mean_psi: mean_psi + np.random.uniform(low=-np.pi, high=np.pi, size=mean_psi.size),
                            *args, **kwargs):
        """
        Calls pdf with extra parameters. This is needed since
        parameters must either be scalars or broadcasted; arrays
        cannot be used as parameters in pdf()

        Parameters
        ----------
        v : callable
            Takes the default (mean) `v`s and modifies them for this call
        psi : callable
            Takes the default (mean) `psi`s and modifies them for this call
        args, kwargs :
            Passed on directly to self.rvs()

        Returns
        -------
        np.array : Result from self.rvs
        """
        old_v, old_psi = self.v, self.psi
        self.v, self.psi = v(self.v), psi(self.psi)
        self._ns = np.arange(1, len(self.v) + 1)
        res = self.pdf(*args, **kwargs)
        self.v, self.psi = old_v, old_psi
        self._ns = np.arange(1, len(self.v) + 1)
        return res
