import numpy as np
import itertools as its

from .utils import wrap_2pi


class Multiplets():
    def __init__(self, nbins, ndims):
        self.nbins = nbins
        self.ndims = ndims
        self.h = np.zeros(shape=[nbins] * ndims)

    def process_event(self, phis):
        """
        Fill the internal histogram with the multiplets of the given `phi` values

        Parameters
        ----------
        phis : np.array
            Phi values of one event
        ```
        """
        _phis = wrap_2pi(phis)
        if self.ndims == 3:
            for i, phi1 in enumerate(_phis):
                # To many combinations of triplets; iterate through phi1s
                chain = its.chain(_phis[i + 1:])
                _tmp = np.array(list(its.combinations(chain, 2)))
                if len(_tmp.shape) == 2:
                    _tmp = np.concatenate([_tmp, np.full((_tmp.shape[0], 1), phi1)], axis=1)
                    _h, _ = np.histogramdd(_tmp,
                                           range=[[0, 2 * np.pi]] * self.ndims,
                                           bins=self.nbins)
                    self.h += _h
        elif self.ndims == 2:
            # doublets can be computed in one go
            _h, _ = np.histogramdd(np.array(list(its.combinations(_phis, 2))),
                                   range=[[0, 2 * np.pi]] * self.ndims,
                                   bins=self.nbins)
            self.h += _h
