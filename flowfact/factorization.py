import numpy as np
from scipy.optimize import least_squares


def make_multiplet_pdf(single_pdf, ndim):
    """
    Given a single particle PDF, calculate the doublet or triplet PDF.

    This can be used with a least square fit to find the single
    particle pdf producing a given measurement.

    Parameters
    ----------
    single_pdf : np.array
        Single particle PDF
    ndim : int
        Dimensionality of the requested multiplet. May be `2` or `3`
    """
    _spdf = np.array(single_pdf)
    if ndim == 2:
        return np.array([np.mean(_spdf * np.roll(_spdf, j)) for j in range(_spdf.size)])
    elif ndim == 3:
        _tmp = np.array([np.roll(_spdf, j)[None, :] * np.roll(_spdf, j)[:, None]
                         for j in range(_spdf.size)])
        _tmp *= _spdf[:, None, None]
        _tmp = np.mean(_tmp, axis=0)
        return _tmp
    else:
        raise ValueError("Only doublets and triplets are currently supported")


def find_single_pdf(multiplet_pdf, **kwargs):
    """
    Find a single particle PDF that can reproduce the given multiplet PDF.

    Parameters
    ----------
    multiplet_pdf : np.ndarray
        The multiplet PDF. Dimensionality gives the type of
        multiplet. If 2 dim its a doublet, if 3 dim its a triplet.

    kwargs :
        Additional arguments to be passed on to scipy.optimize.least_squares

    Returns
    -------
    np.array :
        The found 1D single particle PDF

    Raises
    ------
    ValueError :
        If least_squares fit failed
    """
    ndim = multiplet_pdf.ndim
    reduced_pdf = reduce_dimensionality(multiplet_pdf)

    def model(x):
        return make_multiplet_pdf(x, ndim)

    def fun(x, measured):
        return (model(x) - measured).flatten()

    res = least_squares(fun, 0.001 * np.random.random(reduced_pdf.shape[0]),
                        bounds=(0.0, 1), args=(reduced_pdf, ), **kwargs)
    return res.x


def reduce_dimensionality(multiplet_pdf):
    """
    Using the symmetry along the diagon, reduce the dimensionality of the given PDF
    """
    if multiplet_pdf.ndim == 3:
        rolled_slices = []
        for i, h_slice in enumerate(multiplet_pdf):
            rolled_slices.append(np.roll(np.roll(h_slice, -i, axis=0), -i, axis=1))
        # The sum is essentially an integral over that dimension, so we multiply with the bin width
        reduced_pdf = np.sum(rolled_slices, 0) * (2 * np.pi / multiplet_pdf.shape[0])
    elif multiplet_pdf.ndim == 2:
        rolled_slices = []
        for i, h_slice in enumerate(multiplet_pdf):
            rolled_slices.append(np.roll(h_slice, -i, axis=0))
        reduced_pdf = np.sum(rolled_slices, 0) * (2 * np.pi / multiplet_pdf.shape[0])
    else:
        raise ValueError("Only 2 and 3 dimensional PDFs are supported")
    return reduced_pdf
