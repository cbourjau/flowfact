# Toy MC for Flow like particles

## Usage
Generation of a factorizable event of N particles

```python
import flowfact
from flowfact.utils import wrap_2pi

# Default v_n coefficients starting at v_1
v = np.array([0, 0.3, 0.1])
# Default event planes for each mode; starting at v_
psi = np.array([0, 0, 0])
# Create the flow generator object
flow = flowfact.Flow_gen(v, psi)

# Create an event with 1000 particles using the default v and psi values
# You could also pass lambda-functions here which introduce fluctuations
# around the default values.
phis = wrap_2pi(flow.rvs_with_parameters(lambda v: v, lambda psi: psi, size=1000))
```

## Non-flow
In this context, non-flow describes a non-factorizable fraction of the produced particles. I.e., particles which are not emitted independently. A bivariate distribution can be used for this purpose:

``` python
from scipy.stats import multivariate_normal
# Create a bi-variate random variable
non_flow = multivariate_normal([0, 0], [[0.09, 0.06], [0.06, 0.09]])
# create 100 non-factorizable particles
phis_non_flow = non_flow.rvs(size=100).flatten()
# Combine the flow and non-flow particles to have fun with your analysis
```

