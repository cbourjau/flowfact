from setuptools import setup

install_requires = ['numpy', 'scipy']
tests_require = ['nose']

setup(
    name='flowfact',
    version='0.0.1',
    description="Toy Monte Carlo generator for investigating methods of factorizing flow like multip particle distributions",
    author='Christian Bourjau',
    author_email='christian.bourjau@cern.ch',
    packages=['flowfact'],
    keywords=['alice'],
    # scripts=glob('scripts/*'),
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Mathematics",
        "Topic :: Scientific/Engineering :: Physics",
        "Development Status :: 3 - Alpha",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    install_requires=install_requires,
    extras_require={'test': tests_require},
    test_suite='nose.collector',
)
